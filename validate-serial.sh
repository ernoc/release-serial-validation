#!/bin/bash
#
# Validates Ubuntu Server release serial numbers.
# You can see some examples here:
# http://cloud-images.ubuntu.com/releases/16.04/
# Valid release serial numbers take the form of YYYYMMDD or
# YYYYMMDD.#a release serial number. Usage example:
#
#     validate-serial.sh 20160420.3
#
# Exit codes:
#     0: The script ran correctly and the provided serial is valid.
#         "Ok" is printed to stdout
#     2: Wrong args
#     3: The script ran correctly and the provided serial is not valid.
#        An error message is printed to stderr.
#


DATE_IN=$1

if [ -z "$DATE_IN" ]; then
    >&2 echo "Wrong usage: serial argument missing"
    exit 2
fi

# I assume that '#' in the given format 'YYYYMMDD.#' represents
# "1 or more digits". For security, up to 10 digits are allowed.
# Looking at the examples, ".0" is not valid.
SERIAL_REGEX='^([0-9]{4})([0-9]{2})([0-9]{2})(\.([0-9]{1,10}))?$'

# control input length:
if [ ${#DATE_IN} -le 19 ]; then
    # check syntax and extract fields:
    if [[ $DATE_IN =~ $SERIAL_REGEX ]]; then
        YEAR="${BASH_REMATCH[1]}"
        MONTH="${BASH_REMATCH[2]}"
        DAY="${BASH_REMATCH[3]}"
        NUM="${BASH_REMATCH[5]}"

        if [ "$YEAR" -le "0" ]; then
            >&2 echo "Invalid date: year must be positive"
            exit 3
        fi

        # Convert date to ISO 8601 to avoid any ambiguity with U.S month/day order
        ISO8601_DATE="$YEAR-$MONTH-$DAY"

        # use date command to validate date
        date -d $ISO8601_DATE > /dev/null
        STATUS=$?
        if [ $STATUS -ne 0 ]; then
            # date command already prints an "invalid date message"
            exit 3
        else
            # date OK, validate number
            if [ ! -z "$NUM" ] && [ "$NUM" -le "0" ]; then
                >&2 echo "Invalid number: must be positive"
                exit 3
            else
                echo "Ok"
                # success, exits with code 1
            fi
        fi
    else
        >&2 echo "Invalid format"
        exit 3
    fi
else
    >&2 echo "Invalid format: input too long"
    exit 3
fi
