"""
System-wide test: test the main script validateserial.py:
exit codes and error messages.
"""
from subprocess import Popen, PIPE

import pytest
import sys

from serialvalidation import validateserial


def run_script(serial):
    """Runs the script and returns an (exit_code, stdout, stderr) tuple"""
    process = Popen([sys.executable, validateserial.__file__, serial],
                    stdout=PIPE, stderr=PIPE)
    out, err = process.communicate(input=None)
    exit_code = process.returncode
    return exit_code, out, err


@pytest.mark.system_test
@pytest.mark.parametrize('serial', [
    '20170312',
    '20170312.5'
    # exhaustive positive cases not necessary here -
    # already covered in validateunittests.py
])
def test_exit_code_0(serial):
    # Exit code 0 and no stderr:
    exit_code, actual_stdout, actual_stderr = run_script(serial)
    assert exit_code == 0
    assert 'ok' in actual_stdout.lower()
    assert actual_stderr == ''


@pytest.mark.system_test
@pytest.mark.parametrize('serial, expected_stderr', [
    ('2017312', 'Invalid format'),
    ('20170229', 'Invalid date'),
    ('20170312.0', 'Invalid number')
    # exhaustive positive cases not necessary here -
    # already covered in validateunittests.py
])
def test_exit_code_3(serial, expected_stderr):
    exit_code, actual_stdout, actual_stderr = run_script(serial)
    assert exit_code == 3
    assert actual_stdout == ''
    assert expected_stderr.lower() in actual_stderr.lower()


@pytest.mark.system_test
def test_bad_args():
    """Test serial argument missing"""
    process = Popen([sys.executable, validateserial.__file__])
    process.communicate(input=None)
    assert process.returncode == 2
