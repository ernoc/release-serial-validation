from distutils.core import setup

setup(
    name='serialvalidation',
    version='0.1.0',
    packages=['serialvalidation'],
    url='',
    license='',
    author='ernesto-ocampo',
    author_email='ocampoernesto@gmail.com',
    description='Validation of Ubuntu Server release serial numbers.',
    install_requires=[
        'pytest',
    ]
)
