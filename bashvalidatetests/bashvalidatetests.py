"""
This module runs tests on the validate-serial.sh script.
Some tests in this module are very similar to tests used for the
python script. However, the bash script is a separate product,
that may evolve independently, so it's wise to have separate tests
even if they look similar initially. Note that there is nothing
in the code linking these two products together.
"""
import os
import threading

import pytest
try:
    from Queue import Queue, Empty
except ImportError:
    from queue import Queue
from subprocess import Popen, PIPE

from serialvalidation.tests.validateunittests import generate_positive_cases


def run_bash_standalone_script(*args):
    """Runs the script and returns an (exit_code, stdout, stderr) tuple"""
    path_to_script = os.path.realpath(
        os.path.join(__file__,
                     os.pardir,  # package dir
                     os.pardir,  # module dir
                     'validate-serial.sh'))
    process = Popen([path_to_script] + list(args),
                    stdout=PIPE, stderr=PIPE)
    out, err = process.communicate(input=None)
    exit_code = process.returncode
    return exit_code, out, err


def positive_test_worker(job_queue, results_queue):
    try:
        while True:
            serial = job_queue.get(block=False)  # raise exception if empty
            exit_code, out, err = run_bash_standalone_script(serial)
            results_queue.put((exit_code, out, err))
            job_queue.task_done()
    except Empty:
        pass  # no more work to do


@pytest.mark.system_test
@pytest.mark.parametrize('use_dot', [False, True])
def test_positive_cases(use_dot):
    """Test positive examples.

    Tests all dates in an n-year window around today.
    Window size defined by YEAR_SPAN.
    :param use_dot: if True, test YYYYMMMDD.# format, otherwise,
    test YYYYMMDD format
    """
    # we need to multithread here or it takes forever:
    job_queue = Queue(maxsize=0)
    results_queue = Queue(maxsize=0)
    pending = 0
    for serial in generate_positive_cases(use_dot):
        job_queue.put(serial)
        pending += 1

    for i in range(16):  # number of threads
        thread = threading.Thread(target=positive_test_worker,
                                  kwargs=dict(job_queue=job_queue,
                                              results_queue=results_queue))
        thread.setDaemon(True)
        thread.start()

    job_queue.join()

    # get results:
    while pending:
        exit_code, out, err = results_queue.get()
        assert exit_code == 0
        assert 'ok' in out.lower()
        assert err == ''
        pending -= 1


@pytest.mark.system_test
@pytest.mark.parametrize('value', [
    'hello',
    '20170312extrachars',
    'extrachars20170312',
    'release-20170312',  # "release" is not part of the serial number
    '20170312.',
    '20170312.a',         # non-number after .
    '20170312.-1',        # non-number after .
    '20170312.1.2',       # non-number after .
    '2017312',            # wrong format, too short
    '201732.1',           # wrong format with same length as YYYYMMDD
    '2017/3/2',           # wrong format with same length as YYYYMMDD
])
def test_invalid_syntax(value):
    exit_code, out, err = run_bash_standalone_script(value)
    assert exit_code == 3
    assert out == ''
    assert "invalid format" in err.lower()


@pytest.mark.system_test
def test_too_long():
    exit_code, out, err = run_bash_standalone_script('20170312.12345678901234')
    assert exit_code == 3
    assert out == ''
    assert "too long" in err.lower()


@pytest.mark.system_test
@pytest.mark.parametrize('value', [
    '20171301',  # Invalid month
    '20170332',  # Invalid day
    '00000312',  # Invalid year
    '20170229',  # 2017 is not a leap year
])
def test_invalid_date(value):
    exit_code, out, err = run_bash_standalone_script(value)
    assert exit_code == 3
    assert out == ''
    assert "invalid date" in err.lower()

    exit_code, out, err = run_bash_standalone_script(value + '.5')
    assert exit_code == 3
    assert out == ''
    assert "invalid date" in err.lower()


@pytest.mark.system_test
def test_invalid_number():
    # number cannot be 0
    exit_code, out, err = run_bash_standalone_script('20170312.0')
    assert exit_code == 3
    assert out == ''
    assert "invalid number" in err.lower()


@pytest.mark.system_test
def test_bad_args():
    """Test serial argument missing"""
    exit_code, out, err = run_bash_standalone_script()
    assert exit_code == 2
    assert out == ''
    assert "wrong usage" in err.lower()
